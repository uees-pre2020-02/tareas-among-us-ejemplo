﻿namespace Tareas_Among_Us_Ejemplos
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.rbtnHome = new System.Windows.Forms.RadioButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.rbtnTask1 = new System.Windows.Forms.RadioButton();
            this.rbtnTask2 = new System.Windows.Forms.RadioButton();
            this.rbtnTask3 = new System.Windows.Forms.RadioButton();
            this.rbtnTask4 = new System.Windows.Forms.RadioButton();
            this.panelMain = new System.Windows.Forms.Panel();
            this.tlpMain.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tlpMain
            // 
            this.tlpMain.ColumnCount = 2;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 151F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tlpMain.Controls.Add(this.panelMain, 1, 0);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(0, 0);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 1;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Size = new System.Drawing.Size(800, 450);
            this.tlpMain.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.Black;
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.rbtnHome, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.pictureBox1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.rbtnTask1, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.rbtnTask2, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.rbtnTask3, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.rbtnTask4, 0, 5);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 7;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 191F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(151, 450);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // rbtnHome
            // 
            this.rbtnHome.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnHome.Checked = true;
            this.rbtnHome.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.rbtnHome.FlatAppearance.BorderSize = 2;
            this.rbtnHome.FlatAppearance.CheckedBackColor = System.Drawing.Color.Firebrick;
            this.rbtnHome.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.rbtnHome.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Maroon;
            this.rbtnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnHome.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnHome.ForeColor = System.Drawing.Color.White;
            this.rbtnHome.Image = global::Tareas_Among_Us_Ejemplos.Properties.Resources.Home_24px;
            this.rbtnHome.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rbtnHome.Location = new System.Drawing.Point(5, 196);
            this.rbtnHome.Margin = new System.Windows.Forms.Padding(5);
            this.rbtnHome.Name = "rbtnHome";
            this.rbtnHome.Size = new System.Drawing.Size(141, 35);
            this.rbtnHome.TabIndex = 0;
            this.rbtnHome.TabStop = true;
            this.rbtnHome.Text = "       Inicio";
            this.rbtnHome.UseVisualStyleBackColor = true;
            this.rbtnHome.CheckedChanged += new System.EventHandler(this.rbtnHome_CheckedChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::Tareas_Among_Us_Ejemplos.Properties.Resources.GameTile;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(145, 185);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // rbtnTask1
            // 
            this.rbtnTask1.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnTask1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.rbtnTask1.FlatAppearance.BorderSize = 2;
            this.rbtnTask1.FlatAppearance.CheckedBackColor = System.Drawing.Color.Firebrick;
            this.rbtnTask1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.rbtnTask1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Maroon;
            this.rbtnTask1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnTask1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnTask1.ForeColor = System.Drawing.Color.White;
            this.rbtnTask1.Location = new System.Drawing.Point(5, 241);
            this.rbtnTask1.Margin = new System.Windows.Forms.Padding(5);
            this.rbtnTask1.Name = "rbtnTask1";
            this.rbtnTask1.Size = new System.Drawing.Size(141, 35);
            this.rbtnTask1.TabIndex = 0;
            this.rbtnTask1.Text = "Tarea 1";
            this.rbtnTask1.UseVisualStyleBackColor = true;
            this.rbtnTask1.CheckedChanged += new System.EventHandler(this.rbtnTask1_CheckedChanged_1);
            // 
            // rbtnTask2
            // 
            this.rbtnTask2.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnTask2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.rbtnTask2.FlatAppearance.BorderSize = 2;
            this.rbtnTask2.FlatAppearance.CheckedBackColor = System.Drawing.Color.Firebrick;
            this.rbtnTask2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.rbtnTask2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Maroon;
            this.rbtnTask2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnTask2.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnTask2.ForeColor = System.Drawing.Color.White;
            this.rbtnTask2.Location = new System.Drawing.Point(5, 286);
            this.rbtnTask2.Margin = new System.Windows.Forms.Padding(5);
            this.rbtnTask2.Name = "rbtnTask2";
            this.rbtnTask2.Size = new System.Drawing.Size(141, 35);
            this.rbtnTask2.TabIndex = 0;
            this.rbtnTask2.Text = "Tarea 2";
            this.rbtnTask2.UseVisualStyleBackColor = true;
            this.rbtnTask2.CheckedChanged += new System.EventHandler(this.rbtnTask2_CheckedChanged);
            // 
            // rbtnTask3
            // 
            this.rbtnTask3.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnTask3.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.rbtnTask3.FlatAppearance.BorderSize = 2;
            this.rbtnTask3.FlatAppearance.CheckedBackColor = System.Drawing.Color.Firebrick;
            this.rbtnTask3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.rbtnTask3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Maroon;
            this.rbtnTask3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnTask3.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnTask3.ForeColor = System.Drawing.Color.White;
            this.rbtnTask3.Location = new System.Drawing.Point(5, 331);
            this.rbtnTask3.Margin = new System.Windows.Forms.Padding(5);
            this.rbtnTask3.Name = "rbtnTask3";
            this.rbtnTask3.Size = new System.Drawing.Size(141, 35);
            this.rbtnTask3.TabIndex = 0;
            this.rbtnTask3.Text = "Tarea 3";
            this.rbtnTask3.UseVisualStyleBackColor = true;
            this.rbtnTask3.CheckedChanged += new System.EventHandler(this.rbtnTask3_CheckedChanged);
            // 
            // rbtnTask4
            // 
            this.rbtnTask4.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtnTask4.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.rbtnTask4.FlatAppearance.BorderSize = 2;
            this.rbtnTask4.FlatAppearance.CheckedBackColor = System.Drawing.Color.Firebrick;
            this.rbtnTask4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Red;
            this.rbtnTask4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Maroon;
            this.rbtnTask4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtnTask4.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtnTask4.ForeColor = System.Drawing.Color.White;
            this.rbtnTask4.Location = new System.Drawing.Point(5, 376);
            this.rbtnTask4.Margin = new System.Windows.Forms.Padding(5);
            this.rbtnTask4.Name = "rbtnTask4";
            this.rbtnTask4.Size = new System.Drawing.Size(141, 35);
            this.rbtnTask4.TabIndex = 0;
            this.rbtnTask4.Text = "Tarea 4";
            this.rbtnTask4.UseVisualStyleBackColor = true;
            this.rbtnTask4.CheckedChanged += new System.EventHandler(this.rbtnTask4_CheckedChanged);
            // 
            // panelMain
            // 
            this.panelMain.BackColor = System.Drawing.Color.Transparent;
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(154, 3);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(643, 444);
            this.panelMain.TabIndex = 1;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tlpMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.Text = "Tareas de Among Us";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.tlpMain.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.RadioButton rbtnHome;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.RadioButton rbtnTask1;
        private System.Windows.Forms.RadioButton rbtnTask2;
        private System.Windows.Forms.RadioButton rbtnTask3;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.RadioButton rbtnTask4;
    }
}

