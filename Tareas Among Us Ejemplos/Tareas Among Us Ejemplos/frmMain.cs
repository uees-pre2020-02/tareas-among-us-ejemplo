﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tareas_Among_Us_Ejemplos
{
    public partial class frmMain : Form
    {
        UserControl ucHome1 = new ucHome();
        UserControl ucoTask1 = new ucTask1(); 
        UserControl ucoTask2 = new ucTask2();
        UserControl ucoTask3 = new ucTask3();
        UserControl ucoTask4 = new ucTask4();
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            panelMain.Controls.Add(ucHome1);
            panelMain.Controls.Add(ucoTask1);
            panelMain.Controls.Add(ucoTask2);
            panelMain.Controls.Add(ucoTask3);
            panelMain.Controls.Add(ucoTask4);

            ucHome1.Dock = DockStyle.Fill;
            ucoTask1.Dock = DockStyle.Fill;
            ucoTask2.Dock = DockStyle.Fill;
            ucoTask3.Dock = DockStyle.Fill;
            ucoTask4.Dock = DockStyle.Fill;

            ucHome1.Show();
            ucoTask1.Hide();
            ucoTask2.Hide();
            ucoTask3.Hide();
            ucoTask4.Hide();
        }

        private void rbtnTask1_CheckedChanged(object sender, EventArgs e)
        {
            
        }

        private void rbtnHome_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rbtn = (RadioButton) sender;
            ucHome1.Visible = rbtn.Checked;
        }

        private void rbtnTask1_CheckedChanged_1(object sender, EventArgs e)
        {

            RadioButton rbtn = (RadioButton)sender;
            ucoTask1.Visible = rbtn.Checked;
        }

        private void rbtnTask2_CheckedChanged(object sender, EventArgs e)
        {

            RadioButton rbtn = (RadioButton)sender;
            ucoTask2.Visible = rbtn.Checked;
        }

        private void rbtnTask3_CheckedChanged(object sender, EventArgs e)
        {

            RadioButton rbtn = (RadioButton)sender;
            ucoTask3.Visible = rbtn.Checked;
        }

        private void rbtnTask4_CheckedChanged(object sender, EventArgs e)
        {

            RadioButton rbtn = (RadioButton)sender;
            ucoTask4.Visible = rbtn.Checked;
        }
    }
}
